 //Create Death count

 var currentCheckpoint = 0;
 var currentGravity = 0;
 var currentSize = 0;
 var speedMultiplier = 0.9; //3.9
 //
 var respawnTimer = 10;
 //var globalTimer = 0;
 var lastDirection = 0;
 var maxRespawn = 75;
 var debugCollision = 0;
 var timer = 0;
 
 var timeTaken = 0;
 var timeEnable = 1;
 var deathCount = 0;
 // miscellaneous variables
 var score = 0;
 World.frameRate = 45;
 var speed = 5;
 var jumpForce = 5;
 var gravity = 2.5;
 var spawnx=1000;
 var spawny=1000;
 var canmove=0;
 var touchingGround = false;
 var currentGravity = 100;
 var downTime = 0;
 var timeAlive = 0;
 var setTime = 0;
 var jumpCounter = 0;
 // sprites
 var titlebackground=createSprite(200, 200);
 titlebackground.setAnimation("titlescreen");
 titlebackground.scale= 1;
 var wizard=createSprite(spawnx, spawny);
 var ground=createSprite(spawnx, spawny);
 var textBlock=createSprite(90, 370);
 textBlock.visible = false;
 textBlock.scale = 0.5;
 var barrier=createSprite(spawnx, spawny);
 barrier.setAnimation("barrier");
 ground.setCollider("rectangle", 0, 0, 400, 90);
 ground.debug = false;
 ground.visible = false;
 var wall=createSprite(spawnx, spawny);
 wall.setAnimation("wall");
 var wall2=createSprite(spawnx, spawny);
 wall2.setAnimation("wall");
 var enemy=createSprite(spawnx, spawny);
 var getReadyText=createSprite(spawnx, spawny);
 getReadyText.setAnimation("getReady");
 var up=createSprite(spawnx, spawny);
 up.setAnimation("up");
 var down=createSprite(spawnx, spawny);
 down.setAnimation("down");
 var flyMonster=createSprite(spawnx, spawny);
 flyMonster.setAnimation("flyMonster");
 var wizardDuck=createSprite(spawnx, spawny);
 wizardDuck.setAnimation("wizardDuck");
 var wizardPlayer = createSprite(100, 200);
 wizardPlayer.setAnimation("maleWizard");
 wizardPlayer.scale = 0.3;
 var wizardBackground = createSprite(100, 200);
 wizardBackground.setAnimation("heroBackground");
 wizardBackground.scale = 0.9;
 var wizardPlayer2 = createSprite(300, 200);
 wizardPlayer2.setAnimation("femaleWizard");
 wizardPlayer2.scale = 0.3;
 var wizardBackground2 = createSprite(300, 200);
 wizardBackground2.setAnimation("heroBackground");
 wizardBackground2.scale = 0.9;
 function draw() {
   wizard.collide(wall);
   wizard.collide(ground);
   if (enemy.isTouching(wall)) {
     enemy.x = 760;
     enemy.y = 335;
   }
     if (mousePressedOver(wizardBackground)){
     start();
     wizard.setAnimation("maleWizard");
     }
     if (mousePressedOver(wizardBackground2)){
     start();
     wizard.setAnimation("femaleWizard");
     }
     if (wizard.isTouching(textBlock)) {
       timer = timer + 1;
       downTime = downTime + 1;
     }
     if (wizard.isTouching(titlebackground)) {
       setTime = setTime + 1;
     }
     if (setTime === 17) {
       timeAlive = timeAlive + 1;
       setTime = 0;
     }
     function start() {
       wizard.x = 90;
       wizard.y = -10;
       wizard.velocityY = 10;
       wizard.setAnimation("maleWizard");
       wizard.scale = 0.3;
       setGround();
       setBarrier();
       setWall();
       setEnemy();
       text("text", 130, 15);
       wizardBackground2.x = spawnx;
       wizardBackground2.y = spawny;
       wizardPlayer.x = spawnx;
       wizardPlayer.y = spawny;
       wizardBackground.x = spawnx;
       wizardBackground.y = spawny;
       wizardPlayer2.x = spawnx;
       wizardPlayer2.y = spawny;
     }
     function setGround() {
     ground.x = 200;
     ground.y = 420;
     ground.setAnimation("ground");
     ground.scale = 1.3;
         }
 // Create your functions here
   wizard.velocityY += gravity;
   function setBarrier() {
   }
   if (wizard.isTouching(barrier)) {
     wizard.velocityY = 7;
   }
   function setWall() {
     wall.x = -60;
     wall.y = 300;
     wall.visible = false;
     wall2.x = -60;
     wall2.y = 300;
     wall2.visible = false;
   }
   if (ground.isTouching(wizard)) {
       if (keyDown("up") || keyDown("space") || keyDown("w")) {
       wizard.velocityY -= 25;
       jumpCounter += 1;
   }
   }
   if (ground.isTouching(wizard)) {
       if (keyDown("down") || keyDown("s")) {
       downTime = 0;
       wizard.y = 300;
       wizard.debug = false;
       wizard.setCollider("rectangle", 0, 40, 65, 65, 45);
   }
   }
   if (downTime === 30) {
     wizard.x = 90;
     wizard.y = 300;
     wizard.setCollider("rectangle", 0, 0, 235, 285);
     wizard.debug = false;
   }
   function setEnemy() {
     enemy.x = 1000;
     enemy.y = 335;
     enemy.velocityX = -10;
     enemy.setAnimation("fireball");
     enemy.scale = 0.15;
     enemy.bounce(ground);
   }
   if (enemy.isTouching(wizard)) {
     background("black");
     titlebackground.x = 1000;
     titlebackground.y = 1000;
     wizard.x = spawnx;
     wizard.y = spawny;
     enemy.x = spawnx;
     enemy.y = spawny;
     up.x = 1000;
     up.y = 1000;
     down.x = 1000;
     down.y = 1000;
     flyMonster.x = spawnx;
     flyMonster.y = spawny;
     fill(rgb(255, 0, 0, 0.5));
     textSize(25);
     textFont("Courier New");
     text("GAME OVER", 130, 100);
     fill("black");
     fill(rgb(255, 0, 0, 0.5));
     textSize(20);
     text("Time Alive : " + timeAlive, 60, 150);
     textFont("Courier New");
     flyMonster.x = spawnx;
     flyMonster.y = spawny;
     down.x = 1000;
     down.y = 1000;
     textSize(20);
     text("Jump Count : " + jumpCounter, 60, 200);
     textFont("Courier New");
   }
   if (mousePressedOver(wizardBackground)){
     getReadyText.x = 200;
     getReadyText.y = 200;
     getReadyText.scale = 0.3;
     }
     if (mousePressedOver(wizardBackground2)){
     getReadyText.x = 200;
     getReadyText.y = 200;
     getReadyText.scale = 0.3;
     }
   if (timer === 30) {
     getReadyText.x = spawnx;
     getReadyText.y = spawny;
   }
   if (enemy.x === 260) {
     up.x = 200;
     up.y = 200;
     up.scale = 0.1;
   }
     if (enemy.x === 30) {
     up.x = spawnx;
     up.y = spawny;
   }
     if (timer === 175) {
       up.x = spawnx;
       up.y = spawny;
       down.x = 200;
       down.y = 200;
       down.scale = 0.1;
     }
     if (timer === 195) {
       down.x = spawnx;
       down.y = spawny;
     }
     if (timer === 150) {
       enemy.x = spawnx;
       enemy.y = spawny;
       flyMonster.x = 660;
       flyMonster.y = 270;
       flyMonster.velocityX = -10;
       flyMonster.scale = 0.4;
     }
     if (flyMonster.isTouching(wizard)) {
     background("black");
     titlebackground.x = 1000;
     titlebackground.y = 1000;
     wizard.x = spawnx;
     enemy.x = spawnx;
     enemy.y = spawny;
     up.x = spawnx;
     up.y = spawny;
     down.x = spawnx;
     down.y = spawny;
     flyMonster.x = spawnx;
     flyMonster.y = spawny;
     fill(rgb(255, 0, 0, 0.5));
     textSize(25);
     textFont("Courier New");
     text("GAME OVER", 130, 100);
     fill("black");
     fill(rgb(255, 0, 0, 0.5));
     textSize(20);
     text("Time Alive : " + timeAlive, 60, 150);
     textFont("Courier New");
     textSize(20);
     text("Jump Count : " + jumpCounter, 60, 200);
     textFont("Courier New");
     }
   if (timer === 194) {
     enemy.x = 1000;
     enemy.y = 335;
     enemy.velocityX = -10;
     enemy.setAnimation("fireball");
     enemy.scale = 0.15;
     enemy.bounce(ground); 
     }
     // Level 2
   
   // sprites
   var background2 = createSprite(200, 200);
   background2.setAnimation("background2");
   background2.visible = false;
   var ice = createSprite(spawnx, spawny);
   ice.setAnimation("ice");
   
   // functions
   if (timer === 245) {
       timer = 1000;
       enemy.setAnimation("mad_fireball");
       enemy.velocityX = -12.5;
     }
   if (mousePressedOver(wizardPlayer2)) {
     wizard.setAnimation("femaleWizard");
   }
   if (timer === 1040) {
       enemy.setAnimation("mad_fireball");
       enemy.velocityX = -15;
     }
   drawSprites();
   }
   
 